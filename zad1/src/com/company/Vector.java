package com.company;
public class Vector {
    /**
     * funkcja zwraca wylosowany rozmiar wektora z zakresu <5, 10>
     */
    public static int generateSizeOfVector() {
        return (int) (Math.random() * 6 + 5);
    }

    /**
     * funkcja wprowadza dane do wektora. Podczas przetwarzania danych nie
     * mozna korzystac z informacji o rozmiarze wektora. Prosze odpowiednio
     * obsluzyc wyjatek ArrayIndexOutOfBoundsException.
     * Podczas kazdej iteracji losujemy liczbe typu double a nastepnie wprowadzamy
     * ja do naszego wektora - do momentu az przekroczymy rozmiar tablicy i wystapi
     * wyjatek ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void insertDataToVector(double[] vector) {
        try {
            for (int i = 0; ; ++i) {
                vector[i] = Math.random();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Wektor wypełniony");
        }
    }

    /**
     * funkcja wyswietla zawartosc wektora w odpowiednim formacie. Podczas
     * przetwarzania danych nie mozna korzystac z
     * informacji o rozmiarze wektora. Prosze odpowiednio obsluzyc wyjatek
     * ArrayIndexOutOfBoundsException. Podczas kazdej iteracji wyswietlamy kolejny
     * rekord tablicy az przekroczymy jej rozmiar i wystapi wyjatek
     * ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void showVector(double[] vector) {
        try {
            for (int i = 0; ; ++i) {
                System.out.println("TAB[" + i + "]: " + vector[i]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Wektor wyswietlony");
        }
    }

    /**
     * funkcja, w ktorej bedziemy przeprowadzali testy nowych funkcjonalnosci.
     */
    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[generateSizeOfVector()];
        System.out.println("Wprowadzam dane do wektora...");
        insertDataToVector(vector);
        System.out.println("Wyswietlam zawartosc wektora");
        showVector(vector);
    }
}